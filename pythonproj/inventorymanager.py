# for reading files in a dir
from __future__ import print_function
from config.appconfig import *
import random
import sys
import os
import time

# validate paths
def validatepath():
    if not os.path.exists('servers'):
        os.makedirs('servers')

validatepath()

# input file
serverfile_read = open(serverfile, "r+")
serverfile_open = open(serverfile)
# ouput file
serverdata_write = open(serverdata, "a+")

# time ops
TodayDate = (time.strftime('[%Y-%m-%d %H:%M:%S]'))

def file_length(fname):
    with fname as f:
        return sum (1 for _ in f)

def inv_count(sfile):
    return ('%s There are a total of %s servers' % \
           (TodayDate, file_length(sfile)))

def inv_list(sfile):
    readservers = sfile.read()
    return  (readservers)

def log_serverdata(sinput, linput, oinput):
    print (inv_count(oinput), file = sinput)
    print (inv_list(linput), file = sinput)
    print ('%s Operation completed successfully\n---' % (TodayDate), file = sinput)


# run the file
def run():
    log_serverdata(serverdata_write, serverfile_read, serverfile_open)

run()




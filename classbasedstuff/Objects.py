

class Employee(object):
    def __init__(self, Emp_No, Birth_Date, First_Name, Last_Name, Gender, Hire_Date):
        self.Emp_No = Emp_No
        self.Birth_Date = Birth_Date
        self.First_Name = First_Name
        self.Last_Name = Last_Name
        self.Gender = Gender
        self.Hire_Date = Hire_Date


    def give_name(self):
        return ("%s %s" % (self.First_Name, self.Last_Name))

    def DOB(self):
        return ("%s" % (self.Birth_Date))
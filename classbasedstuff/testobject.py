import sys
import os

MyDir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MyDir+"/deps")
os.chdir(MyDir)

import ast
import pymysql

from kms import kms_decrypt_token
from Objects import Employee

'''
uses the mysql employees test DB
AWS KMS for key encryption

'''


EmpNoVar = sys.argv[1]


Query = "Select * from employees where emp_no=" + EmpNoVar

Host = 'AQICAHj0ei5oWM4nAzLGFzQVQYP3hzLUoL6LG8O/3fIJ/zYhigFTPQX7rNIi6NQ+7sjgivN5AAAAZzBlBgkqhkiG9w0BBwagWDBWAgEAMFEGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMzskk8dhQOvqbdEA1AgEQgCT0TDpYslX8PNEPYivZJ/5p5agmC2G9maFIgiqjXgo4pdvaBkg='
DBUserName = 'AQICAHj0ei5oWM4nAzLGFzQVQYP3hzLUoL6LG8O/3fIJ/zYhigGJ1FcJszUj8CX+Ny5jN1yoAAAAYjBgBgkqhkiG9w0BBwagUzBRAgEAMEwGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMc3mW4JIIA5ccbM9UAgEQgB9sCsTEQqJeYwzxyCg0ykPvvy9PF1Nk1DaX0u0HgOoZ'
DBPasswd = 'AQICAHj0ei5oWM4nAzLGFzQVQYP3hzLUoL6LG8O/3fIJ/zYhigF8AWg/sDMx77HW+EaBo5MmAAAAYjBgBgkqhkiG9w0BBwagUzBRAgEAMEwGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMguQdHuSYSPal+6xyAgEQgB8rtjnDBA24WjAW7AjP6gf65zjkQSZitNK9l1Mckpps'


# convert datetime

conv = pymysql.converters.conversions.copy()
conv[246]=float
conv[10]=str

# sql connect

Connection = pymysql.connect(host=kms_decrypt_token(Host),
                             user=kms_decrypt_token(DBUserName),
                             password=kms_decrypt_token(DBPasswd),
                             db='employees',
                             charset='utf8mb4',
                             conv = conv,
                             cursorclass=pymysql.cursors.DictCursor)

with  Connection.cursor() as cursor:
    cursor.execute(Query)
    EmpList = str(cursor.fetchall())
    EmpDictStr = EmpList.strip('[]')
    EmpDict = ast.literal_eval(EmpDictStr)


EmpObject = Employee(EmpDict.get('emp_no'),
                     EmpDict.get('birth_date'),
                     EmpDict.get('first_name'),
                     EmpDict.get('last_name'),
                     EmpDict.get('gender'),
                     EmpDict.get('hire_date'))



print ("%s %s %s" % (Employee.give_name(EmpObject), "was born on", Employee.DOB(EmpObject)))




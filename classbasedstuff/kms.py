import os
import sys

MyDir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MyDir+"/deps")

import boto3
from base64 import b64decode


AwsRegion = 'ap-southeast-2'

def kms_decrypt_token(token):
    kms = boto3.client('kms', region_name=AwsRegion)
    return kms.decrypt(CiphertextBlob=b64decode(token))['Plaintext'].decode('utf-8')
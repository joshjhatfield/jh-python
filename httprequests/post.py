import sys
import os

MyDir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MyDir+"/deps")
os.chdir(MyDir)

import requests
import json
import hmac
import hashlib

from config import *
from kms import kms_decrypt_token

HmacDecrypted = str(kms_decrypt_token(Hmac))


Payload = {
    'Name':Name,
    'FooData':FooData,
    'TestData':TestData
}


HmacSig = hmac.new(HmacDecrypted, json.dumps(Payload), digestmod=hashlib.sha256)

Request = requests.post(Url, headers= {'hmacid':HmacSig.hexdigest()}, data= json.dumps(Payload))



print ( "%s \n\n %s:%s" % (Request.text,'Http Response', Request.status_code))





import random
import sys
import os
import re

# A list, looks just like terraform ! 
hardware_list = ['disk', 'memory', 'cpu',
				'PSU']

# print 2nd item from list remember 0 = 1 !!

print('memory_component', hardware_list[1])

# change list item
hardware_list[2] = "intel-i5"
# print the new change
print('cpu-description', hardware_list[2])

# print multiples
print(hardware_list[0:2])


# another list
software_list = ['Pycharm', 'sublime', 'sourcetree', 'ansible']

# Listception
full_list = [hardware_list, software_list]
print(full_list)

# print cpu + sublime
print((full_list[2][1]))


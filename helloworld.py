import random
import sys
import os
import re

# Hello world

print ("Hello world")

# A variable
name = "helloworld"

print(name)


# Number ops


# Will return a "5 + 2 = 7"
print("5 + 2 =", 5+2)



# A quote 
quote = "\"All People"

Multi_line_quote = ''' Seem to 
Need Data
Processing'''

new_string = quote + Multi_line_quote

# print strings

print("%s %s %s" % ('For tcp model', quote, Multi_line_quote))

# add 5 newlines blank
print('\n' * 5)